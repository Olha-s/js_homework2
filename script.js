const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

const root = document.getElementById('root');
const list = document.createElement('ul');
root.append(list);

function checkBooks(author, name, price) {
  if (author && name && price) {
    const listItem = document.createElement('li');
    list.append(listItem);
    listItem.innerText = `author: ${author}, name: ${name}, price: ${price}`;
    } else if (!author) {
      throw "the book has no author";
    } else if (!name) {
      throw "the book has no name";
    } else {
    throw "the book has no prise";
    }
}

books.forEach(item => {

  try {
    checkBooks(item.author, item.name, item.price)
  } catch (e) {
    console.error(e);
   }

});

